import { Component } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';

@Component({
  selector: 'app-search-input',
  templateUrl: './search-input.component.html',
  styleUrls: ['./search-input.component.scss']
})
export class SearchInputComponent {
  searchKey:string = "";

  constructor(private http:ApiService){}
  
  fetchData(){
    this.http.searchImage();
  }

  setSearchParams() {
    this.http.setSearchParams(this.searchKey);
  }
}
