import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';

@Component({
  selector: 'app-output',
  templateUrl: './output.component.html',
  styleUrls: ['./output.component.scss'],
})
export class OutputComponent implements OnInit {
  imagesData: any = [];
  constructor(private http: ApiService) {}

  ngOnInit(): void {
    this.http.imagesData.subscribe((data) => {
      this.imagesData = data.results;
    });
  }
}
