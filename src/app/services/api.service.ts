import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject } from "rxjs";

@Injectable({
  providedIn: 'root',
})
export class ApiService {
  imagesData = new BehaviorSubject<any>([]);
  searchParams: string = 'cat';

  constructor(private http: HttpClient) {}
  
  setSearchParams(input:string) {
    this.searchParams = input
  }

  searchImage() {
    this.http
      .get(
        `https://api.unsplash.com/search/collections?client_id=S2bPwQqnfJfSc2II6fpVUUOKbSYkJf12gXfL4WOZXtI&query=${this.searchParams}`
      )
      .subscribe((response: any) => {
        this.imagesData.next(response);
        console.log(response);
        
      });
  }
}
